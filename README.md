#  Ext: df_tools

<img src="https://www.sgalinski.de/typo3conf/ext/project_theme/Resources/Public/Images/logo.svg" />

License: [GNU GPL, Version 2](https://www.gnu.org/licenses/gpl-2.0.html)

Repository: https://gitlab.sgalinski.de/typo3/df_tools

Please report bugs here: https://gitlab.sgalinski.de/typo3/df_tools

TYPO3 version: >7.6 

## About
This extension provides some useful tools for testing your internal redirects, 
links on your site, backlinks on other sites and to compare content of two different URLs or the same URL with a previously saved state. 
You manage the whole application with the backend module in the admin section. 

Furthermore you can schedule the tests with the [Scheduler extension](https://docs.typo3.org/typo3cms/extensions/scheduler/).

## Installation
1. Install the extension with the extension manager

2. Add the static template of the extension to your template of your root page or you extension root templates


## Configuration
In the Extension Manager you have various options to configure the Module:

###### Record Storage Page
If you want to store the test records on a specific page/system folder, you can provde a page id here.

---

###### Excluded Tables
A comma seperated list of tables that the **Linkchecker** should ignore, when searching for URLs.

---

###### Exclude Table Fields
A comma seperated list of table columns that the **Linkchecker** should ignore, when searching for URLs.
You need to use the format **table<column>** here.

---

###### Timeout limit
The maximum time the **URL checker** should spend before failing the request.

## The backend module
All the parts of the backend module are used in a similar way (the **Test Links** section behaves slightly different):

You can **add** and **run** new tests with the appropriate buttons. By double clicking a row, you can edit the fields of the selected test.
Each row has an icon to delete, edit or refresh the entry. For additional help you can uncollapse the **Help** section at the top.

The module consists of 4 parts:

###### Test Redirects
This tool can be used to test redirects of different kinds. You can automatically start it by creating a scheduler job.

When adding a test you need to specify the Test URL and the Expected URL. Also you should add the expected HTTP Code (default is 200)

---

###### Test Links
You can use the tool to find and test all link checks on your site. The results are ordered by http result code and the matching of the URLs. Also this job can be triggered by an scheduler job.

With **the Synchronize Data** you update the tests to contain all the links from your site. You need to do this, whenever you add a new link to the site.
Double clicking a row will open a new window with the given URL of that row loaded in it.

---

###### Test Back Links
This tool checks if a configured url contains a backlink to your domain. The task can be executed as a scheduler job too.

Again you can add a **Test URL** and an **Expected URL**.

---

###### Test Content Comparison
If you ever need to compare the content of two pages, then you should use this function. You just need to enter two urls to start the comparison of the contents. Of course you can trigger this job by using a scheduler job.

## Adding a Scheduler Task
If you want to regularly run your tests, you should setup task with the TYPO3 [Scheduler extension](https://docs.typo3.org/typo3cms/extensions/scheduler/).

df_tools comes with multiple **CommandController** tasks:

###### RedirectTest: runAllTests
This task will run all configured redirect tests as configured in the backend module.

---

###### RedirectTest: importFromRealUrl
Creates redirect tests from [realurl](https://typo3.org/extensions/repository/view/realurl) redirect entries.

---

###### ContentComparisonTest: runAllTests
This task will run all configured content comparison tests as configured in the backend module.

---

###### ContentComparisonTest: synchronize
Synchronizes the contents of the content comparison test urls.

---

###### BacklinkTest: runAllTests
This task will run all configured backlink tests as configured in the backend module.

---

###### LinkCheck: runAllTests
This task will run all configured link check tests as configured in the backend module.

---

###### LinkCheck: synchronize
Synchronizes the current data with the external urls on the page.
