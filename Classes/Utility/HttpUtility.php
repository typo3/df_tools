<?php

namespace SGalinski\DfTools\Utility;

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Core\TimeTracker\NullTimeTracker;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\Page\PageGenerator;
use TYPO3\CMS\Frontend\Page\PageRepository;

/**
 * Collection of smaller http utility functions
 */
final class HttpUtility {
	/**
	 * Prefixes a string with the current host if it starts with a slash!
	 *
	 * Note:
	 * The method t3lib_div::locationHeaderUrl does the same, but it's much slower and
	 * doesn't work with installation in sub-directories and if we must handle AJAX or Scheduler
	 * requests.
	 *
	 * @static
	 * @throws \RuntimeException if the current site url could not be determined
	 * @param string $string
	 * @return string
	 */
	public static function prefixStringWithCurrentHost($string) {
		if ($string{0} === '/') {
			if (trim(GeneralUtility::getIndpEnv('HTTP_HOST')) !== '') {
				$locationUrl = GeneralUtility::getIndpEnv('TYPO3_SITE_URL');
			} else {
				if (!is_object($GLOBALS['TSFE']) || !is_array($GLOBALS['TSFE']->config)) {
					self::initTSFE();
				}

				if (trim($GLOBALS['TSFE']->baseUrl) !== '') {
					$locationUrl = $GLOBALS['TSFE']->baseUrl;
				} elseif (trim($GLOBALS['TSFE']->absRefPrefix) === 'auto' ||
					trim($GLOBALS['TSFE']->absRefPrefix) === './'
				) {
					/** @var PageRepository $pageRepository */
					$pageRepository = GeneralUtility::makeInstance(PageRepository::class);

					/** @var $db DatabaseConnection */
					$db = $GLOBALS['TYPO3_DB'];
					$domainRecord = $db->exec_SELECTgetSingleRow(
						'domainName',
						'sys_domain',
						'1=1' . $pageRepository->enableFields('sys_domain'),
						'',
						'pid, sorting',
						''
					);

					$locationUrl = $domainRecord['domainName'];
				} elseif (trim($GLOBALS['TSFE']->absRefPrefix) !== '') {
					$locationUrl = $GLOBALS['TSFE']->absRefPrefix;
				} else {
					throw new \RuntimeException('The current site url could not be determined!');
				}
			}

			$string = trim($locationUrl, ' /') . $string;
		}

		return $string;
	}

	/**
	 * Checks whether a domain record (sys_domain) has been configured for each site root.
	 *
	 * @return NULL|Status An error status is returned for each site root page without domain record.
	 */
	protected function getDomainRecordAvailableStatus() {
		$status = NULL;
		$rootPages = $this->getRootPages();
		$rootPagesWithoutDomain = [];

		$rootPageIds = [];
		foreach ($rootPages as $rootPage) {
			$rootPageIds[] = $rootPage['uid'];
		}

		foreach ($rootPageIds as $rootPageId) {
			if (!array_key_exists($rootPageId, $domainRecords)) {
				$rootPagesWithoutDomain[$rootPageId] = $rootPages[$rootPageId];
			}
		}

		if (!empty($rootPagesWithoutDomain)) {
			foreach ($rootPagesWithoutDomain as $pageId => $page) {
				$rootPagesWithoutDomain[$pageId] = '[' . $page['uid'] . '] ' . $page['title'];
			}

			$status = GeneralUtility::makeInstance(
				'TYPO3\\CMS\\Reports\\Status',
				'Domain Records',
				'Domain records missing',
				'Domain records are needed to properly index pages. The following
	sites are marked as root pages, but do not have a domain configured:
	<ul><li>' . implode(
					'</li><li>',
					$rootPagesWithoutDomain
				) . '</li></ul>',
				Status::ERROR
			);
		}

		return $status;
	}

	/**
	 * Initializes the TSFE object to fetch the configured location url
	 *
	 * Note: Only useful in AJAX or Scheduler environments!
	 *
	 * @static
	 * @return void
	 */
	protected static function initTSFE() {
		$GLOBALS['TT'] = new NullTimeTracker();
		$GLOBALS['TSFE'] = GeneralUtility::makeInstance(
			'TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController', $GLOBALS['TYPO3_CONF_VARS'], 0, 0
		);
		$GLOBALS['TSFE']->initFEuser();
		$GLOBALS['TSFE']->determineId();
		$GLOBALS['TSFE']->initTemplate();
		$GLOBALS['TSFE']->getConfigArray();
		PageGenerator::pagegenInit();
	}
}

?>
