<?php

if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

if (TYPO3_MODE === 'BE') {
	/** @var $_EXTKEY string */
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
		'SGalinski.' . $_EXTKEY,
		'tools',
		'tools',
		'',
		array(
			'Overview' => 'index',
			'RedirectTest' => 'index',
			'RedirectTestCategory' => 'read',
			'LinkCheck' => 'index',
			'BackLinkTest' => 'index',
			'ContentComparisonTest' => 'index',
		),
		array(
			'access' => 'user,group',
			'icon' => 'EXT:df_tools/ext_icon.png',
			'labels' => 'LLL:EXT:df_tools/Resources/Private/Language/locallang_tools.xlf',
		)
	);

	$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
	$iconRegistry->registerIcon('run', \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class, [
		'source' => 'EXT:df_tools/Resources/Public/Icons/run.png'
	]);
}

/** @var $_EXTKEY string */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_dftools_domain_model_redirecttest');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages(
	'tx_dftools_domain_model_redirecttestcategory'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_dftools_domain_model_linkcheck');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_dftools_domain_model_recordset');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_dftools_domain_model_backlinktest');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages(
	'tx_dftools_domain_model_contentcomparisontest'
);
?>